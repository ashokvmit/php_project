<?php

abstract class subClass{
    public abstract function firstFunction($param1);
    abstract function secondFunction($param, $param2);
}


class mainClass extends subClass{
    public function firstFunction($param1)
    {
        echo "First Method = " . $param1 . "<br>";
    }

    public function secondFunction($param, $param2)
    {
        echo "Second Method = " . $param;
        echo "<br> Second Method = " . $param2;
    }
}

$obj = new mainClass();

$obj->firstFunction("Hello World");
$obj->secondFunction("First", "Second");
?>