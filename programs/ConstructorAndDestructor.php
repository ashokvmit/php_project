<?php

class A{
    public $Name;
    public $Age;
    
    function __construct($Name, $Age, $City) {
        $this->Name = $Name;
        $this->Age = $Age;
        echo "<br>Additional : City = " . $City;
        echo "<br>Contructed...";
    }
    
   /*  function __construct($Name, $Age) {
        $this->Name = $Name;
        $this->Age = $Age;
        echo "Contructed...";
    } */
    
  
    
    function print() {
        echo "<br> Name : $this->Name";
        echo "<br> Age : $this->Age" ;
    }
    
    function __destruct() {
        echo "<br> Destructed...";
    }
}

//    $obj = new A("Ashok", 22);
    $obj = new A("Smart", 25, "Erode");
    $obj->print();

?>