<?php
function newLine() {
    echo "<br>";
}


class Fruits{
    private $name;
    public $age;
    protected $mark;
    
    function get_name() {
        return $this ->name ;
    }
    
    function set_name($name) {
        $this->name = $name;
    }
    function get_age() {
        return $this ->age ;
    }
    
    function set_age($age) {
        $this->age = $age;
    }
    function get_mark() {
        return $this ->mark ;
    }
    
    function set_mark($mark) {
        $this->mark = $mark;
    }
}

    $obj1 = new Fruits();
    $obj1->set_name("Ashok");
    $obj1->set_age(22);

    $obj1->set_mark(100);
    newLine();
    echo $obj1->get_name();
    newLine();
    echo $obj1->get_age();
    newLine();
    echo $obj1->get_mark();
    
?>