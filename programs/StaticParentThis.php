<?php

Class A{
    public static $Name = "Ashok";
    
    public function get_Name() {
        echo $this::$Name;
    }
    
}

Class B extends A{
    public static $Name = "Smart";
}

    $obj1 = new B();
    
   $obj1->get_Name();

?>